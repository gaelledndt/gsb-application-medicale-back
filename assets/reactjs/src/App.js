import React, { useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Navbar from "./components/Navbar/Navbar";
import SignIn from "./pages/Auth/SignIn/SignIn";
import Dashboard from "./pages/Dashboard/Dashboard";
import LegalNotice from "./pages/LegalNotice/LegalNotice";
import Allergenic from "./pages/Allergenic/Allergenic";
import Test from "./pages/Test/Test";
import MedicalStatus from "./pages/MedicalStatus/MedicalStatus";
import Parameter from "./pages/Parameters/Parameters";
import Prescription from "./pages/Prescription/Prescription";

const App = () => {
    const [user, setUser] = useState([]);
    console.log('user.allergenic ==>', user?.allergenic);
    console.log('user.prescriptions ==>', user?.prescriptions);
    console.log("JSON Prescriptions",JSON.stringify(user?.prescriptions));


    return (
        <BrowserRouter>
            <Navbar user={user} setUser={setUser} />
            {user.length === 0 ? (
                <Routes>
                    <Route path={'/'} element={<SignIn setUser={setUser} />} />
                </Routes>
            ) : (
                <Routes>
                    <Route path={'/dashboard'} element={<Dashboard user={user} />} />
                    <Route path={'/legalnotice'} element={<LegalNotice />} />
                    <Route
                        path={'/allergenic'}
                        element={<Allergenic allergenic={user?.allergenic} />}
                    />
                    <Route path={'/test'} element={<Test tests={user?.test} />} />
                    <Route path={'/medicalstatus'} element={<MedicalStatus healthStatus={user?.healthStatus} />} />
                    <Route path={'/parameter'} element={<Parameter careSummary={user?.careSummary} />} />
                    <Route
                        path={'/prescription'}
                        element={<Prescription prescriptions={user?.prescriptions} />}
                    />
                </Routes>
            )}
        </BrowserRouter>
    );
};

export default App;
