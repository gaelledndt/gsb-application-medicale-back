import React, { useState } from 'react';
import { Card, CardContent, Typography, Button, Collapse } from '@material-ui/core';
import '../../assets/styles/allergenic/allergenic.css';

function Allergenic({ allergenic }) {
    const [expanded, setExpanded] = useState(null);

    try {
        return (
            <div>
                <h1>Vos allergies:</h1>
                {allergenic?.length !== 0 ? (
                    allergenic.map((allergy, index) => {
                        const showDetails = expanded === index;

                        return (
                            <Card key={index}>
                                <CardContent>
                                    <Typography variant="h5">{allergy.name}</Typography>
                                    {/*<Button onClick={() => setExpanded(showDetails ? null : index)}>{showDetails ? 'Cacher les détails' : 'En savoir +'}</Button>*/}
                                </CardContent>
                              {/*  <Collapse in={showDetails}>
                                    <CardContent>
                                        <Typography variant="h6">Détails :</Typography>
                                        <Typography>Description : {allergy.description}</Typography>
                                        <Typography>Niveau : {allergy.level}</Typography>
                                    </CardContent>
                                </Collapse>*/}
                            </Card>
                        );
                    })
                ) : (
                    <p>Vous n'avez aucune allergie.</p>
                )}
            </div>
        );
    } catch (error) {
        console.error('Une erreur s\'est produite lors de l\'affichage des allergies :', error);
        return <p>Erreur lors de l'affichage des allergies.</p>;
    }
}

export default Allergenic;
