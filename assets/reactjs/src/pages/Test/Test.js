import React from 'react';
import { Card, Typography } from '@material-ui/core';

function Test({ tests }) {
    try {
        return (
            <div>
                <h1>Vos Tests:</h1>
                {tests.length !== 0 ? (
                    tests.map((test, index) => (
                        <Card key={index} style={{marginBottom: "20px"}}>
                            <Typography variant="h5" component="h2" style={{padding: "20px"}}>
                                Test
                            </Typography>
                            <Typography variant="body2" component="p" style={{padding: "20px"}}>
                                <strong>Description :</strong> <br />
                                <div dangerouslySetInnerHTML={{ __html: test.description }}></div> <br />
                                <strong>Commentaire :</strong> <br />
                                <div dangerouslySetInnerHTML={{ __html: test.comment }}></div>
                            </Typography>
                        </Card>
                    ))
                ) : (
                    <p>Aucun test disponible.</p>
                )}
            </div>
        );
    } catch (error) {
        console.error('Une erreur s\'est produite lors de l\'affichage des tests :', error);
        return <p>Erreur lors de l'affichage des tests.</p>;
    }
}

export default Test;
