import React, { useState } from 'react';
import { Card, CardContent, Typography, Button, Collapse } from '@material-ui/core';

function Prescription({ prescriptions }) {
    const [expanded, setExpanded] = useState(null);

    try {
        const prescriptionList = Object.entries(prescriptions).filter(([key, value]) => key !== 'prescriptionMedications' && value.beginDate && value.endDate);
        const prescriptionMedications = prescriptions.prescriptionMedications || [];

        return (
            <div>
                <h1>Vos Prescriptions:</h1>
                {prescriptionList.length !== 0 ? (
                    <div>
                        {prescriptionList.map(([key, prescription], index) => {
                            const beginDate = new Date(prescription.beginDate.date).toLocaleString();
                            const endDate = new Date(prescription.endDate.date).toLocaleString();
                            const showDetails = expanded === index;

                            return (
                                <Card key={index}>
                                    <CardContent>
                                        <Typography variant="h5">Prescription</Typography>
                                        <Typography>Début : {beginDate}</Typography>
                                        <Typography>Fin : {endDate}</Typography>
                                        <Button onClick={() => setExpanded(showDetails ? null : index)}>{showDetails ? 'Cacher les détails' : 'En savoir +'}</Button>
                                    </CardContent>
                                    {prescriptionMedications[index] &&
                                        <Collapse in={showDetails}>
                                            <CardContent>
                                                <Typography variant="h6">Médication de prescription :</Typography>
                                                <Typography>Médication : {prescriptionMedications[index].medication}</Typography>
                                                <Typography>Description : {prescriptionMedications[index].description}</Typography>
                                                <Typography>Début : {new Date(prescriptionMedications[index].beginDate.date).toLocaleString()}</Typography>
                                                <Typography>Fin : {new Date(prescriptionMedications[index].endDate.date).toLocaleString()}</Typography>
                                            </CardContent>
                                        </Collapse>
                                    }
                                </Card>
                            );
                        })}
                    </div>
                ) : (
                    <p>Aucune prescription n'a été trouvée.</p>
                )}
            </div>
        );
    } catch (error) {
        console.error('Une erreur s\'est produite lors du rendu du composant Prescription :', error);
        return <p>Une erreur s'est produite lors du chargement des prescriptions.</p>;
    }
}

export default Prescription;
