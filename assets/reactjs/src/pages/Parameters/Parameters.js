import React from 'react';
import { Card, Typography } from '@material-ui/core';

function Parameter({ careSummary }) {
    try {
        return (
            <div>
                <h1>Vos informations personnelles :</h1>
                <Card style={{ marginBottom: "20px", padding: "20px" }}>
                    <Typography variant="body2" component="p">
                        Prénom : {careSummary.firstname}
                    </Typography>
                    <Typography variant="body2" component="p">
                        Nom : {careSummary.lastname}
                    </Typography>
                    <Typography variant="body2" component="p">
                        Numéro de sécurité sociale : {careSummary.numberSs}
                    </Typography>
                    <Typography variant="body2" component="p">
                        Date de naissance : {new Date(careSummary.birthday.date).toLocaleDateString()}
                    </Typography>
                    <Typography variant="body2" component="p">
                        Description : <div dangerouslySetInnerHTML={{ __html: careSummary.description }}></div>
                    </Typography>
                </Card>
            </div>
        );
    } catch (error) {
        console.error('Une erreur s\'est produite lors de l\'affichage des informations personnelles :', error);
        return <p>Erreur lors de l'affichage des informations personnelles.</p>;
    }
}

export default Parameter;
