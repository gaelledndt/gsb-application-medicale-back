import React, { useState } from 'react';
import { Card, CardContent, Typography, Button, Collapse } from '@material-ui/core';
import '../../assets/styles/legalnotice/legalnotice.css';

function MedicalStatus({ healthStatus }) {
    const [expanded, setExpanded] = useState(null);

    try {
        return (
            <div>
                <h1>Votre statut médical</h1>
                {healthStatus.length !== 0 ? (
                    healthStatus.map((status, index) => {
                        const showDetails = expanded === index;

                        return (
                            <Card key={index}>
                                <CardContent>
                                    <Typography variant="h5">Maladie : {status.disease}</Typography>
                                    <Button onClick={() => setExpanded(showDetails ? null : index)}>{showDetails ? 'Cacher les détails' : 'En savoir +'}</Button>
                                </CardContent>
                                <Collapse in={showDetails}>
                                    <CardContent>
                                        <Typography variant="h6">Détails :</Typography>
                                        <Typography>Description : {status.description}</Typography>
                                        <Typography>Statut : {status.status}</Typography>
                                    </CardContent>
                                </Collapse>
                            </Card>
                        );
                    })
                ) : (
                    <p>Aucun statut médical trouvé.</p>
                )}
            </div>
        );
    } catch (error) {
        console.error('Une erreur s\'est produite lors de l\'affichage du statut médical :', error);
        return <p>Erreur lors de l'affichage du statut médical.</p>;
    }
}

export default MedicalStatus;
